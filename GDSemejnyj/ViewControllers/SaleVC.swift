//  SaleVC.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit
import WebKit

class SaleVC: UIViewController {
    
    @IBOutlet weak var saleWebKitView: WKWebView!
    @IBOutlet weak var contactButton: UIButton!
    
    
    @IBAction func contactPopButton(_ sender: UIButton) {
        let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpContactVC") as! PopUpContactVC
        
        self.addChild(popUpVC)
        popUpVC.view.frame = self.view.frame
        self.view.addSubview(popUpVC.view)
        
        popUpVC.didMove(toParent: self)
    }
    
    @IBAction func homeNavig(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: UrlList().urlSaleStr)!
        let request = URLRequest(url: url)
        saleWebKitView.load(request)
        
        contactButton.layer.cornerRadius = 5
        contactButton.layer.borderColor = UIColor.gray.cgColor
        contactButton.layer.borderWidth = 0.2
        contactButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        contactButton.layer.shadowOpacity = 0.7
        contactButton.layer.shadowColor = UIColor.gray.cgColor
        contactButton.layer.shadowRadius = 5

    }

}
