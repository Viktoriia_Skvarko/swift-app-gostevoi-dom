//  PopUpContactVC.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit

class PopUpContactVC: UIViewController {
    
    var urlList = UrlList()

    @IBOutlet weak var viewPopUpOutlet: UIView!
    @IBOutlet weak var iconViberPopUp: UIImageView!
    @IBOutlet weak var iconMessegPopUp: UIImageView!
    @IBOutlet weak var iconTelegrPopUp: UIImageView!
    
    @IBAction func dismisButton(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let recognizerTapViber = UITapGestureRecognizer(target: self, action: #selector(tapViber(_:)))
        iconViberPopUp.addGestureRecognizer(recognizerTapViber)
        let recognizerTapMessege = UITapGestureRecognizer(target: self, action: #selector(tapMessege(_:)))
        iconMessegPopUp.addGestureRecognizer(recognizerTapMessege)
        let recognizerTapTelegram = UITapGestureRecognizer(target: self, action: #selector(tapTelegram(_:)))
        iconTelegrPopUp.addGestureRecognizer(recognizerTapTelegram)
        
        viewPopUpOutlet.layer.cornerRadius = 15
        viewPopUpOutlet.layer.borderColor = UIColor.gray.cgColor
        viewPopUpOutlet.layer.borderWidth = 0.5
        
    }
    
    @objc func tapViber(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlViberStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func tapMessege(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlMessengStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func tapTelegram(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlITelegStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

}
