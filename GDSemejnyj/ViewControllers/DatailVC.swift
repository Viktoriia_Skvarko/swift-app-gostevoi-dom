//  DatailVC.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit

class DatailVC: UIViewController {
    
    var informDetail: OneRoom?
    var indexForDetail: Int?
    
    let itemsPerRow = 3
    let margin: CGFloat = 5
    
    @IBOutlet weak var nameRoom: UILabel!
    @IBOutlet weak var descriptionRoom: UILabel!
    @IBOutlet weak var priceRoom: UILabel!
    
    @IBOutlet weak var collectionDetail: UICollectionView!
    @IBOutlet weak var connectButton: UIButton!
    
    
    @IBAction func homeNavigButton(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = informDetail!.name
        nameRoom.text = informDetail!.name
        descriptionRoom.text = informDetail!.description
        priceRoom.text = informDetail!.price
        
        connectButton.layer.cornerRadius = 5
        connectButton.layer.borderColor = UIColor.gray.cgColor
        connectButton.layer.borderWidth = 0.2
        connectButton.layer.shadowOffset = CGSize(width: 5, height: 5)
        connectButton.layer.shadowOpacity = 0.7
        connectButton.layer.shadowColor = UIColor.gray.cgColor
        connectButton.layer.shadowRadius = 5
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "imageSegue" else { return }
        
        if let cell = sender as? UICollectionViewCell,
           let indexPath = collectionDetail.indexPath(for: cell),
           let vc = segue.destination as? ImageZoomVC {
            
            let itemNomer = indexForDetail!
            vc.imageDetailZooming = RoomsList.roomsDescription[itemNomer].imagesDetailList.imageDetailList[indexPath.item]
          

        }
    }
    
    
    
    @IBAction func popUpContactAction(_ sender: UIButton) {
        
        let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpContactVC") as! PopUpContactVC
        
        self.addChild(popUpVC)
        popUpVC.view.frame = self.view.frame
        self.view.addSubview(popUpVC.view)
        
        popUpVC.didMove(toParent: self)
        
    }
    
}

extension DatailVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let itemNomer = indexForDetail!
        return RoomsList.roomsDescription[itemNomer].imagesDetailList.imageDetailList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let itemNomer = indexForDetail!
        let item = RoomsList.roomsDescription[itemNomer].imagesDetailList.imageDetailList[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCellDetail", for: indexPath) as! PhotoCellDetail
        cell.imageDetail.image = UIImage(named: item.image)
        return cell
        
    }
}


extension DatailVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, canEditItemAt indexPath: IndexPath) -> Bool {
        return true
    }
}


extension DatailVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (UIScreen.main.bounds.width - CGFloat(itemsPerRow + 1) * margin) / CGFloat(itemsPerRow)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 15, left: 0, bottom: margin, right: 0)
    }
    
}
