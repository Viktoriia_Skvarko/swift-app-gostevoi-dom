//  ImageZoomVC.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit

class ImageZoomVC: UIViewController {
    
    var imageDetailZooming: ImageDetail?
    
    @IBOutlet weak var imZoom: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imZoom.image = UIImage(named: imageDetailZooming!.image)
        
        imZoom.layer.cornerRadius = 5
        imZoom.layer.borderColor = UIColor.gray.cgColor
        imZoom.layer.borderWidth = 0.3
    }
}
