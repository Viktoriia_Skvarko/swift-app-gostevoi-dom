//  ViewController.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit

class StartVC: UIViewController {
    
    @IBOutlet weak var iconRooms: UIImageView!
    @IBOutlet weak var iconSale: UIImageView!
    @IBOutlet weak var iconPlaces: UIImageView!
    @IBOutlet weak var iconContacts: UIImageView!
    
    let radiusImage = CGFloat(10)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        iconRooms.layer.cornerRadius = radiusImage
        iconSale.layer.cornerRadius = radiusImage
        iconPlaces.layer.cornerRadius = radiusImage
        iconContacts.layer.cornerRadius = radiusImage
        
    }

}

