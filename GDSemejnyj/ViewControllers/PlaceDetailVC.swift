//  PlaceDetailVC.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit

class PlaceDetailVC: UIViewController {
    
    var informDetail: Place?
    
    @IBOutlet weak var imagePlaceDetail: UIImageView!
    @IBOutlet weak var namePlaceDetail: UILabel!
    @IBOutlet weak var distancePlaceDetail: UILabel!
    @IBOutlet weak var discriptPlaceDetail: UITextView!
    
    @IBOutlet weak var connectButtonInPlace: UIButton!
    
    @IBAction func homeNavigButton(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func popUpContactInPlace(_ sender: UIButton) {
        let popUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpContactVC") as! PopUpContactVC
        
        self.addChild(popUpVC)
        popUpVC.view.frame = self.view.frame
        self.view.addSubview(popUpVC.view)
        
        popUpVC.didMove(toParent: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePlaceDetail.image = UIImage(named: informDetail!.imageName)
        namePlaceDetail.text = informDetail!.namePlace
        distancePlaceDetail.text = informDetail!.distantion
        discriptPlaceDetail.text = informDetail!.description
        
        connectButtonInPlace.layer.cornerRadius = 5
        connectButtonInPlace.layer.borderColor = UIColor.gray.cgColor
        connectButtonInPlace.layer.borderWidth = 0.2
        connectButtonInPlace.layer.shadowOffset = CGSize(width: 5, height: 5)
        connectButtonInPlace.layer.shadowOpacity = 0.7
        connectButtonInPlace.layer.shadowColor = UIColor.gray.cgColor
        connectButtonInPlace.layer.shadowRadius = 5
        
    }

}
