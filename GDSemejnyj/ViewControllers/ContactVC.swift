//  ContactVC.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit

class ContactVC: UIViewController {
    
    var urlList = UrlList()
    
    @IBOutlet weak var iconInstagram: UIImageView!
    @IBOutlet weak var iconFb: UIImageView!
    @IBOutlet weak var iconVk: UIImageView!
    @IBOutlet weak var iconOk: UIImageView!
    @IBOutlet weak var iconViber: UIImageView!
    @IBOutlet weak var iconMassege: UIImageView!
    @IBOutlet weak var iconTlm: UIImageView!
    
    var radiusSocial = CGFloat(14)
    var radiusMesseger = CGFloat(25)
    var widthBorder = CGFloat(1)
    var colorBorder = UIColor.gray.cgColor
    
    
    @IBAction func homeNavigButton(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let recognizerTapInst = UITapGestureRecognizer(target: self, action: #selector(tapInstagram(_:)))
        iconInstagram.addGestureRecognizer(recognizerTapInst)
        let recognizerTapFb = UITapGestureRecognizer(target: self, action: #selector(tapFb(_:)))
        iconFb.addGestureRecognizer(recognizerTapFb)
        let recognizerTapVk = UITapGestureRecognizer(target: self, action: #selector(tapVk(_:)))
        iconVk.addGestureRecognizer(recognizerTapVk )
        let recognizerTapOk = UITapGestureRecognizer(target: self, action: #selector(tapOk(_:)))
        iconOk.addGestureRecognizer(recognizerTapOk)
        let recognizerTapViber = UITapGestureRecognizer(target: self, action: #selector(tapViber(_:)))
        iconViber.addGestureRecognizer(recognizerTapViber)
        let recognizerTapMessege = UITapGestureRecognizer(target: self, action: #selector(tapMessege(_:)))
        iconMassege.addGestureRecognizer(recognizerTapMessege)
        let recognizerTapTelegram = UITapGestureRecognizer(target: self, action: #selector(tapTelegram(_:)))
        iconTlm.addGestureRecognizer(recognizerTapTelegram)
        
        iconInstagram.layer.cornerRadius = radiusSocial
        iconFb.layer.cornerRadius = radiusSocial
        iconVk.layer.cornerRadius = radiusSocial
        iconOk.layer.cornerRadius = radiusSocial
        iconViber.layer.cornerRadius = radiusMesseger
        iconMassege.layer.cornerRadius = radiusMesseger
        iconTlm.layer.cornerRadius = radiusMesseger
        
        iconInstagram.layer.borderWidth = widthBorder
        iconFb.layer.borderWidth = widthBorder
        iconVk.layer.borderWidth = widthBorder
        iconOk.layer.borderWidth = widthBorder
        iconViber.layer.borderWidth = widthBorder
        iconMassege.layer.borderWidth = widthBorder
        iconTlm.layer.borderWidth = widthBorder
        
        iconInstagram.layer.borderColor = colorBorder
        iconFb.layer.borderColor = colorBorder
        iconVk.layer.borderColor = colorBorder
        iconOk.layer.borderColor = colorBorder
        iconViber.layer.borderColor = colorBorder
        iconMassege.layer.borderColor = colorBorder
        iconTlm.layer.borderColor = colorBorder

    }
    
    @objc func tapInstagram(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlInstaStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func tapFb(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlIFbStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func tapVk(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlVkStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func tapOk(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlOkStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func tapViber(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlViberStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func tapMessege(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlMessengStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @objc func tapTelegram(_ recognizer: UITapGestureRecognizer) {
        let url = URL(string: urlList.urlITelegStr)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

}
