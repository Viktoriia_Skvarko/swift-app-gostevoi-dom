//  CollectionViewCell.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit

class PlaceCell: UICollectionViewCell {
    
    @IBOutlet weak var imagePlace: UIImageView!
    @IBOutlet weak var namePlaceText: UILabel!
    
}
