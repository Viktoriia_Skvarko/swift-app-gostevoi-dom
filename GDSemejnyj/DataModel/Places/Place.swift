//  PlaceOne.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import Foundation

struct Place {
    var imageName: String
    var namePlace: String
    var distantion: String
    var description: String
}
