//  UrlList.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import Foundation

struct UrlList {
    
    var urlInstaStr = "https://www.instagram.com/gostevoi_dom_semeinii/"
    var urlIFbStr = "https://www.facebook.com/PopovkaKrim"
    var urlVkStr = "https://vk.com/popovka_krim"
    var urlOkStr = "https://ok.ru/popovka.krim"
    var urlViberStr = "viber://chat?number=%2B380679882459"
    var urlMessengStr = "http://m.me/PopovkaKrim"
    var urlITelegStr = "https://t.me/dariabulgakova"
    var urlSaleStr = "https://popovka.top/info/65-sale-inform"
}
