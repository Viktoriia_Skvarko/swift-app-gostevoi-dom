//  PhotoCell.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var textRoomsMain: UILabel!
    
}
