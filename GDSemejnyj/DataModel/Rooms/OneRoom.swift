//  OneNomerModel.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import Foundation

struct OneRoom {
    var name: String
    var imageName: String
    var description: String
    var price: String
    var imagesDetailList: ImageDetailList
}
