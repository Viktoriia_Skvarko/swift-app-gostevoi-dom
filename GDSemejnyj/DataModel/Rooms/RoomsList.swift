//  RoomsList.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import Foundation
import UIKit

struct RoomsList {
    
    static let roomsDescription: Array<OneRoom> = [
        
        OneRoom(name: "Номер Двухкомнатный",
                imageName: "nomerdvuhkomnatnii",
                description: "В номере две комнаты, свой санузел. В первой комнате диван и односпальная кровать, телевизор, кондиционер, шкаф, стул, тумбочка. Во второй - двухспальная кровать / две односпальные кровати, тумбочки, шкаф, холодильник.",
                price: "Цена: от 3800 руб",
                imagesDetailList: ImageDetailList(imageDetailList: [ImageDetail(image: "nomerdvuhkomnatnii"),
                                                                    ImageDetail(image: "dvuhkomnatnii1"),
                                                                    ImageDetail(image: "dvuhkomnatnii2"),
                                                                    ImageDetail(image: "dvuhkomnatnii3"),
                                                                    ImageDetail(image: "dvuhkomnatnii4"),
                                                                    ImageDetail(image: "dvuhkomnatnii5"),
                                                                    ImageDetail(image: "territoriya1"),
                                                                    ImageDetail(image: "territoriya2"),
                                                                    ImageDetail(image: "territoriya3")]
                )
        ),
        
        OneRoom(name: "Номер Полулюкс",
                imageName: "nomerlux",
                description: "В номере 1 комната площадью 25 м. кв. Разделена на две части перегородкой. В номерах есть двухспальные или односпальные кровати. Вид из номера на зону отдыха и бассейн. Свой санузел (туалет, умывальник), шкаф, прикроватные тумбы, стул.",
                price: "Цена: от 3800 руб",
                imagesDetailList: ImageDetailList(imageDetailList: [ImageDetail(image: "nomerlux"),
                                                                    ImageDetail(image: "polulux1"),
                                                                    ImageDetail(image: "polulux2"),
                                                                    ImageDetail(image: "polulux3"),
                                                                    ImageDetail(image: "polulux4"),
                                                                    ImageDetail(image: "polulux5"),
                                                                    ImageDetail(image: "territoriya1"),
                                                                    ImageDetail(image: "territoriya2"),
                                                                    ImageDetail(image: "territoriya3")]
                )
        ),
        
        OneRoom(name: "Номер Семейный",
                imageName: "nomersemeinii",
                description: "В номере 1 комната площадью 16-20 м. кв. Рассчитана на 3-4 человека (есть с двухспальными или односпальными кроватями). Вид из номера на зону отдыха и фонтан. В номере свой санузел (туалет, умывальник), шкаф, прикроватные тумбы, стул.",
                price: "Цена: от 3400 руб",
                imagesDetailList: ImageDetailList(imageDetailList: [ImageDetail(image: "nomersemeinii"),
                                                                    ImageDetail(image: "semeinii1"),
                                                                    ImageDetail(image: "semeinii2"),
                                                                    ImageDetail(image: "semeinii3"),
                                                                    ImageDetail(image: "semeinii4"),
                                                                    ImageDetail(image: "semeinii5"),
                                                                    ImageDetail(image: "territoriya1"),
                                                                    ImageDetail(image: "territoriya2"),
                                                                    ImageDetail(image: "territoriya3")]
                )
        ),
        
        OneRoom(name: "Номер Мансардный",
                imageName: "nomermansard",
                description: "В номере 1 комната площадью 14-18 м. кв. Рассчитана на двоих. Есть номера с двухспальными и односпальными кроватями. Вид из номера на море и бассейн. В номере свой санузел (туалет, умывальник), шкаф, прикроватные тумбы, стул.",
                price: "Цена: от 2300 руб",
                imagesDetailList: ImageDetailList(imageDetailList: [ImageDetail(image: "nomermansard"),
                                                                    ImageDetail(image: "mansard1"),
                                                                    ImageDetail(image: "mansard2"),
                                                                    ImageDetail(image: "mansard3"),
                                                                    ImageDetail(image: "mansard4"),
                                                                    ImageDetail(image: "mansard5"),
                                                                    ImageDetail(image: "territoriya1"),
                                                                    ImageDetail(image: "territoriya2"),
                                                                    ImageDetail(image: "territoriya3")]
                )
        ),
        
        OneRoom(name: "Номер Комфорт",
                imageName: "nomerkomfort",
                description: "В номере 1 комната площадью 14-18 м. кв. Рассчитана на 2-3 человека (есть с двухспальными или односпальными кроватями). Вид из номера на детскую площадку и бассейн. В номере свой санузел (туалет, умывальник), шкаф, прикроватные тумбы, стул.",
                price: "Цена: от 2300 руб",
                imagesDetailList: ImageDetailList(imageDetailList: [ImageDetail(image: "nomerkomfort"),
                                                                    ImageDetail(image: "komfort1"),
                                                                    ImageDetail(image: "komfort2"),
                                                                    ImageDetail(image: "komfort3"),
                                                                    ImageDetail(image: "komfort4"),
                                                                    ImageDetail(image: "komfort5"),
                                                                    ImageDetail(image: "territoriya1"),
                                                                    ImageDetail(image: "territoriya2"),
                                                                    ImageDetail(image: "territoriya3")]
                )
        ),
        
        OneRoom(name: "Номер Эконом",
                imageName: "nomereconom",
                description: "В номере 1 комната площадью 14-17 м. кв. Рассчитана на 2-4 человека (есть с двухспальными или односпальными кроватями). Вид из номера на зону отдыха и фонтан. В номере свой санузел (туалет, умывальник), шкаф, прикроватные тумбы, стул.",
                price: "Цена: от 1700 руб",
                imagesDetailList: ImageDetailList(imageDetailList: [ImageDetail(image: "nomereconom"),
                                                                    ImageDetail(image: "econom1"),
                                                                    ImageDetail(image: "econom2"),
                                                                    ImageDetail(image: "econom3"),
                                                                    ImageDetail(image: "econom4"),
                                                                    ImageDetail(image: "econom5"),
                                                                    ImageDetail(image: "territoriya1"),
                                                                    ImageDetail(image: "territoriya2"),
                                                                    ImageDetail(image: "territoriya3")]
                )
        )
    ]
    
    static func itemRoom(forIndex index: Int) -> OneRoom {
        return roomsDescription[index % roomsDescription.count]
    }
    
}
