//  AppDelegate.swift
//  GDSemejnyj
//  Created by Viktoriia Skvarko


import UIKit
import UserNotifications
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
    
    var window: UIWindow?
    
    var center: UNUserNotificationCenter {
        UNUserNotificationCenter.current()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // запрос на уведомления и локальные и удаленные:
        center.getNotificationSettings { setting in
            if setting.authorizationStatus != .authorized {
                requestAuthorization()
            }
        }
        
        // регистрируемся на сервере APNs:
        UIApplication.shared.registerForRemoteNotifications()
        
        
        func requestAuthorization() {
            let options: UNAuthorizationOptions = [.alert, .sound, .badge]
            self.center.requestAuthorization(options: options) { (didAllow, error) in
                // действия если пользователь разрешил
                print("Разрешение на отправку уведомлений:", didAllow)
                // действия если пользователь запретил
                if !didAllow {
                    print("Запрет отправки уведомлений:")
                }
            }
        }
        
        return true
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // генерация токена
        let token = deviceToken.map { String(format: "%02hhx", $0) }.joined()
        print("Token:", token)
        
        // отправляем токен на сервер Firebase в базу данных
        let ref: DatabaseReference! = Database.database().reference()
        ref.child("tokens").child(token).setValue(["token": token])
        
    }
    
    
    // регистрация токена для приема сообщений от Firebase
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber += 1
    }
    
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
}

